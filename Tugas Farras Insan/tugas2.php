<?php 
  
	function terbilang ($rupiah) { //function terbilang adalah fungsi yang digunakan untuk mengelompokan atau membedakan bilangan yang akan dimasukan pada variabel rupiah. rupiah adalah variabel yang digunakan sebagai wadah untuk menyimpan data atau value yang akan diproses atau variabel yang digunakan untuk menjalankan fungsi dari function terbilang 
		$angka = array(); //array adalah kode yang berisi beberapa nilai dengan tipe data yang sama. variabel angka digunakan untuk menyimpan, mendeskripsikan atau menampilkan. 
		$angka = ["","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan","sepuluh","sebelas"];//dalam variabel angka terdapat beberapa data yang berada didalam kurung siku, yang dimulai dari " " karna 0, 0 karna array memulai angka dari 0 dilanjut dengan "satu","dua" dan seterusnya

		if ($rupiah < 12)// if adalah jika angka yang dimasukan pada variabel rupiah kurang dari dua belas
			return " " . $angka[$rupiah];//Karna variabel rupiah kurang dari dua belas maka data yang akan di tampilkan dari variabel rupiah adalah data yang tersimpan pada variabel angka.

		elseif ($rupiah < 20) // elseif berfungsi menampilkan jika variabel rupiah kurang dari dua puluh
			return  terbilang ($rupiah - 10)." "."belas";//Proses yang akan dilakukan pada tahap ini yaitu rupiah dikurangi sepuluh lalu ditambah string belas
		elseif ($rupiah < 100)//elseif berfungsi menampilkan jika variabel rupiah kurang dari seratus
			return terbilang ($rupiah/10)." "."puluh". terbilang ($rupiah % 10);//Proses yang akan dilakukan pada tahap ini yaitu rupiah dibagi sepuluh ditambah string puluh, lalu untuk sisanya rupiah modulus sepuluh
		elseif ($rupiah < 200) {//elseif berfungsi menampilkan jika variabel rupiah kurang dari seratus
			return " seratus" . terbilang($rupiah - 100);
		}//karna bilangan seratus ini spesial maka sebelum proses harus ditambah string seratus, lalu untuk proses nya adalah variabel rupiah dikurangi seratus
		elseif ($rupiah < 1000)//elseif berfungsi menampilkan jika variabel rupiah kurang dari seribu
			return terbilang ($rupiah/100)." "."ratus". terbilang ($rupiah % 100);//Proses yang akan dilakukan pada tahap ini yaitu rupiah dibagi seratus ditambah string ratus, lalu untuk sisanya rupiah modulus seratus
		elseif ($rupiah < 2000){//elseif berfungsi menampilkan jika variabel rupiah kurang dari dua ribu
			return  " seribu" . terbilang($rupiah - 1000);
		}//karna bilangan seribu ini spesial maka sebelum proses harus ditambah string seribu, lalu untuk proses nya adalah variabel rupiah dikurangi seribu
		elseif ($rupiah < 1000000)//elseif berfungsi menampilkan jika variabel rupiah kurang dari satu juta
			return terbilang ($rupiah/1000)." "."ribu".terbilang ($rupiah % 1000);//Proses yang akan dilakukan pada tahap ini yaitu rupiah dibagi seribu ditambah string ribu, lalu untuk sisanya rupiah modulus seribu
		elseif ($rupiah < 1000000000)//elseif berfungsi menampilkan jika variabel rupiah kurang dari satu miliyar
			return terbilang ($rupiah/1000000)." "."juta".terbilang ($rupiah % 1000000);//Proses yang akan dilakukan pada tahap ini yaitu rupiah dibagi satu juta ditambah string juta, lalu untuk sisanya rupiah modulus satu juta
		elseif ($rupiah < 1000000000000)//elseif berfungsi menampilkan jika variabel rupiah kurang dari satu Triliun
			return terbilang ($rupiah/1000000000)." "."miliyar".terbilang(fmod($rupiah, 1000000000));//Proses yang akan dilakukan pada tahap ini yaitu rupiah dibagi satu miliyar ditambah string miliyar, lalu untuk sisanya rupiah modulus satu miliyar

				// Return terbilang bersifat rekursif magsudnya adalah untuk menampilkan atau memanggil hasil dari proses yang telah dikerjakan pada tahap sebelumnya. Contohnya jika variabel rupiah = 150 maka angka 50 dipanggil dari proses rupiah dibagi 10

				// Hasil yang ditampilkan dari pembagian, modulus, dan pengurangan variabel rupiah diambil dari data yang tersimpan dari variabel angka 

				// return berfungsi memanggil data tanpa menampilkannya.

				// fmod adalah fungsi yang sama dari modulus tetapi fungsi ini tidak terbatas

				// "%" modulus hanya menampilkan sisa bagi yang kurang dari miliyar
				
				// " " didalam kutip dua berfungsi sebagai spasi dan titik sebagai asigment atau penggabungan. 
	}

	echo terbilang ();//echo berfungsi untuk menampilkan hasil yang ingin ditanpilkan dari fungsi terbilang yang disimpan di dalam kurung " (); "
	
	?>