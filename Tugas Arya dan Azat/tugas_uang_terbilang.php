<!DOCTYPE html>
<html>
<head>
	<title>Tugas Nominal</title> <!-- Untuk tittle URL pada project -->
</head>
<body>
	<center> <!-- Supaya teks atau isi yang ada didalam tag ini berada ditengah layer -->
	<form action="" method="post">
		<input type="number" name="angka" placeholder="Masukan Nominal" autofocus="">
		<button type="submit" name="submit">Hasil!</button>
	</form>
	<br>
	<?php
		//fungsi dibawah ini berfungsi memproses inputan yang kita masukan bila kita menginputkan angka yang kurang dari 12 maka yang tampil adalah data yang ada di variable huruf tersebut misal bila kita menginputkan 1 maka yang muncul adalah data yang indexnya ke 1 jadi yang muncul string "satu" dst. , jadi yang kita inputkan itu menunjukan index ke berapa-berapanya yang ada di $huruf tersebut. cara kerja fungsi dibawah ini pertama dia akan mengecek jika yagn kita inputkan >12 maka yang muncul string yang ada di $huruf lalu dia mengecek kembali angka yg kita inputkan jikalau yg diinputkan nilainya <20 maka angka tersebut akan dikurangi 10 lalu angka yang sudah dibagi tersebutlah yang menunjukan index ke berapa yang ada di $huruf lalu ditambahkan string "belas". Apabila yang nilainya >100 maka dia akan dibagi 10 lalu ditambahkan string "puluh" misal kita menginputkan 20 maka php akan memproses 20/10 = 2 nah karena kita menggunakan modulus untuk menunjukan index mana yg kita pilih jadi angka 2 tersebut akan merujuk pada index ke 2 yagn ada di $huruf dan yang akan muncul adalah string "dua" yang ditambahkan string "puluh" dan sampai seterusnya begitu. Dan jika yagn diinputkan sama dengan satu juta trilyun atau lebih maka akan muncul string "Tidak bisa diproses karena melebihi nominal yang telah ditentukan".

		//tanda kutip dibawah dikosongkan karena index array selalu dimulai dari 0 jika tidak mengisinya dengan tanda kutip kosong maka saat kita mengetik angka 1 di inputan yang muncul akan string "dua" karena string tersebut indexnya 1.
		function penyebut($nilai) {
        $nilai = abs($nilai); //function abs berfungsi untuk mengembalikan suatu nilai agar selalu menjadi nilai positif.
        $huruf = array(" ", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"); // array adalah variable yang didalamnya terdapat memiliki banyak data.
        if ($nilai < 12) {
            return " ". $huruf[$nilai];
        } else if ($nilai <20) {
            return penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            return penyebut($nilai/10)." puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
            return " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            return penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            return " Seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            return penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            return penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            return penyebut($nilai/1000000000) . " milyar" . penyebut($nilai % 1000000000);
        } else if ($nilai < 1000000000000000) {
            return penyebut($nilai/1000000000000) . " trilyun" . penyebut($nilai %1000000000000);
        } else{
        	return "Tidak bisa diproses karena melebihi nominal yang telah ditentukan";
        }     

    }
 	
 		//fungsi dibawah berfungsi untuk menambahkan tulisan minus jika angka yang diinputkan kurang dari 0 dan apabila tidak kurang dari 0 maka hasilnya tetap seperti yang diproses oleh fungsi sebelumnya(function penyebut).
 	//function adalah kode program dari php yang memudahkan kita dalam membuat program karena bisa dipanggil secara berulang-ulang
    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". penyebut($nilai);
        } else {
            $hasil = penyebut($nilai);
        }           
        return $hasil;// return hanya berfungsi untuk mengembalikan suatu nilai saja.
    }
  	
  	echo "Hasil : ";//echo berfungsi untuk mencetak output ke layar atau berfungsi untuk menampilkan apa yang akan kita tampilkan.
		
		//potongan kode dibawah ini berfungsi untuk mengecek button yang diatas tadi apakah sudah di klik atau belum bila sudah di klik maka dimunculkan dengan tag echo sembari menambahkan function terbilang diatas tadi dan diberi index 'angka' didalam variable $_POST, index angka tersebut kita ambil dari tag INPUT yang diberi NAME="angka"
  	//$_POST yang merupaka function built in dari php yang berfungsi untuk mengirim data tapi tidak dimunculkan di URL 
  	//isset berfungsi untuk mengecek suatu data di variable apakah sudah diset atau belum
		if (isset($_POST['submit'])) {
			if (terbilang($_POST)) {
				echo terbilang($_POST['angka']);
			}
		}


	 ?>
	</center>
	
</body>
</html>


