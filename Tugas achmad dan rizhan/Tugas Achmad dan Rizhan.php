<!DOCTYPE html>
<html>
<head>
	<title>
		<!-- mengubah tittle atau judul dari web yang dibuat -->
		USIA ANDA
	</title>
</head>
<body>
<form method="post">
	<!-- tag form untuk membuat tempat menginput dan menggunakan metode post yang berfungsi mengirim data ke pemanggil data input -->
	<tr>
		<!-- tag tr untuk membuat baris tabel -->
		<td>MASUKKAN TANGAAL LAHIR ANDA</td>
			<!-- tag td unuk membuat isi dari tabel -->
		<br>
			<!-- untuk menghentikan laju data berikutnya atau membuat garis baru -->
		<td><input type="date" name="waktu"></td>
			<!-- tag input untuk membuat sebuah input dengan type date atau tanggal dan diberi nama untuk dipanggil di fungsi berikutnya --> 
	</tr>
	<tr>
    	<td colspan="3"><input type="submit" value="Simpan"></td>
 			<!-- membuat tombol dengan type submit untuk memasukan atau mengkonfirmasi data yang sebelumnya dimasukkan dengan  -->
 	</tr>
 	<br><br>
</form>
<?php 
	$waktu = $_POST['waktu'];
		//memanggil data yang telah dimasukkan, data tersebut berupa array yang berisi tahun, bulan, dan tanggal lalu di panggil dengan fungsi post 
	$tahun = new DateTime($waktu);
		//membuat fungsi zona waktu yang telah di masukkan ke dalam variabel sebelumnya(zona waktu lokal)
	$tgl   = new DateTime();
		//membuat fungsi zona waktu yang ada di sistem komputer atau mengambil zona waktu yang terdapat pada sistem komputer

	$usia  = date_diff($tgl , $tahun);
		//membuat fungsi perbandingan zona waktu yang sebelumnya telah ditentukan 
	$tahun = $usia -> y;
		//mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan tahun
	$bulan = $usia -> m;
		//mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan bulan
	$hari  = $usia -> d;
		//mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan hari atau tanggal
	$jam   = $usia -> h;
		//mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan jam
	$menit = $usia -> i;
		//mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan menit
	$detik = $usia -> s;
		//mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan detik

	echo (" USIA ANDA: " . "</br>" . $tahun . " Tahun " . $bulan . " Bulan " . $hari . " Hari " . $jam . " Jam " . $menit . " Menit " . $detik . " Detik")
		//memanggil satu persatu variabel yang telah terisi data dari masing masing jenis waktu(hari, tahun, dsb)

 ?>

</body>
</html>