<?php 
	function penyebut($rupiah) { 
	// Function adalah potongan kode yang mempermudah kita dalam membuat projek, function yang kita pakai disini "penyebut", function yang kita gunakan adalah tipe user defined. Sedangkan $rupiah adalah parameternya. 

		$rupiah = abs($rupiah);
		 // fungsi abs yaitu mengembalikan nilai positif, contohnya kalau yang diinputkan di parameternya itu nilai negatif, maka akan dikembalikan menjadi nilai positif. $rupiah adalah variabel yang kita gunakan.
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"); 
		// karena array dimulai dari angka nol, maka kutip pertama dari isi array kami kosongkan.
		$temp = ""; 
		// $temp berfungsi untuk menambahkan array yang belum ada diatas, serta menambahkan $huruf yang sudah kita dimasukan dalam array.
		if ($rupiah < 12) {
			$temp = " ". $huruf[$rupiah];
		// jika	angka yang kita masukan kurang dari 12 maka, yang akan ditampikan yaitu $huruf yang sudah berada di array.	
		} else if ($rupiah <20) { 
			$temp = penyebut($rupiah - 10). " belas"; 
		// jika angka yang dimasukan kurang dari 20, maka angka yang diinputkan tadi akan dikurang 10 kemudian hasilnya digabungkan dengan string. Contohnya bila angka yang kita masukan 18, delapan belas tadi akan dikurangi dulu sepuluh menjadi delapan lalu digabungkan dengan string "belas" menjadi delapan belas.	
		} else if ($rupiah < 100) {
			$temp = penyebut($rupiah/10)." puluh". penyebut($rupiah % 10);
		// jika angka yang kita masukan kurang dari 100, maka angka yang dinputkan tadi akan dibagi 10 kemudian hasilnya akan digabungkan dengan string, jika angka tersebut memiliki sisa bagi maka sisa bagi itu akan ditambahkan setelah string. Contohnya bila angka yang dimasukan 23, dua puluh tiga tadi akan dibagi dulu sepuluh menjadi dua lalu diambahkan string "puluh", karena ada sisa bagi maka sisa bagi itu akan tampil dibelakang sring, menjadi dua puluh tiga.	
		} else if ($rupiah < 200) {
			$temp = " seratus" . penyebut($rupiah - 100);
		// jika angka yang dimasukan kurang dari dua ratus, maka angka yang dinputkan akan dikurangi 100 dan ditambahkan string didepannya. Contohnya angka yang dimasukan 170, seratus tujuh puluh tadi akan dikurangi seratus menjadi tujuh puluh kemudian tujuh puluh tadi digabungkan dengan "seratus"	menjadi seratus tujuh puluh.
		} else if ($rupiah < 1000) {
			$temp = penyebut($rupiah/100) . " ratus" . penyebut($rupiah % 100);
		// jika angka yang dimasukan kurang dari seribu, maka angka yang diinputkan tadi akan dibagi 100 kemudian hasilnya akan digabungkan dengan string "ratus", bila angka yang diinputkan memiliki sisa bagi maka sisa bagi tersebut akan ditambahkan dibelakang "ratus".	
		} else if ($rupiah < 2000) {
			$temp = " seribu" . penyebut($rupiah - 1000);
		// jika angka yang dimasukan kurang dari dua ribu, maka angka yang diinputkan tadi akan dukurangi 1000 dan didepannya ditambahkan "seribu".	
		} else if ($rupiah < 1000000) {
			$temp = penyebut($rupiah/1000) . " ribu" . penyebut($rupiah % 1000);
		// jika angka yang diinputkan kurang dari 1000000, maka angka yang diinputkan tadi akan dibagi 1000 kemudian hasilnya akan digabungkan dengan string "ribu", jika angka yang diinputkan memiliki sisa bagi maka sisa bagi tersebut akan ditambahkan dibelakang string "ribu".	
		} else if ($rupiah < 1000000000) {
			$temp = penyebut($rupiah/1000000) . " juta" . penyebut($rupiah % 1000000);
		// jika angka yang diinputkan kurang dari 1000000000, maka angka yang diinputkan tadi akan dibagi 1000000 kemudian hasilnya akan digabungkan dengan string "juta", jika angka yang diinputkan memiliki sisa bagi maka bagi tersebut akan ditambahkan dibelakang string "juta".
		} else if ($rupiah < 1000000000000) {
			$temp = penyebut($rupiah/1000000000) . " milyar" . penyebut(fmod($rupiah,1000000000)); 
		// jika angka yang diinputkan kurang dari 1000000000000, maka angka yang diinputkan tadi akan dibagi 1000000000 kemudian hasilnya akan digabungkan dengan string "milyar", jika angka yang diinputkan memiliki sisa bagi maka sisa bagi tersebut akan ditambahkan dibelakang string "milyar".	
		// fmod berfungsi membuat perhitungan modulus atau sisa bagi.
		} else if ($rupiah < 1000000000000000) {
			$temp = penyebut($rupiah/1000000000000) . " trilyun" . penyebut(fmod($rupiah,1000000000000));
		// jika angka yang diinputkan kurang dari 1000000000000000, maka angka yang diinputkan akan dibagi 1000000000000 kemudian hasilnya akan digabungkan dengan string "trilyun", jika angka yang diinputkan memiliki sisa bagi maka sisa bagi tersebut akan ditambahkan dibelakang string "trilyun".	
		}     
		return $temp; 
		// return berfungsi untuk mengirim value.
	}
 
	function terbilang($rupiah) {
		if($rupiah<0) {
			$hasil = "minus ". trim(penyebut($rupiah)); // trim berfungsi untuk menghilangkan karakter yang diinginkan.
		} else {
			$hasil = trim(penyebut($rupiah));
		}     		
		return $hasil;
	// jika angka yang diinputkan itu negatif maka angka tersebut akan diproses disini dan ditampilkan dalam bentuk negatif pula.	
	}
 
 
	$angka = 2999000;
	echo terbilang($angka); // untuk menampilkan angka yang dimasukan dalam varameter.
	?>