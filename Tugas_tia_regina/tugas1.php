<?php
            
            function terbilang($rupiah){ //function terbilang termasuk pada User defined function.
                $angka = array();//Array adalah wadah variable yang dapat menampung banyak nilai/data yang sama.
                $angka = [" ", "Satu", "Dua", "Tiga",
                 "Empat", "Lima", "Enam", "Tujuh",
                "Delapan","Sembilan", "Sepuluh",
                 "Sebelas", ]; //Kurung siku untuk menyimbolkan array.
                 $iii ="Seratus"; //agar saat masukan 100 maka output nya adalah "seratus" bukan satu ratus.
                 $uuuu = "Seribu"; //agar saat masukan 1000 maka output nya adalah "seribu" bukan satu ribu
                     //membuat user defined function dengan nama "terbilang" agar mudah untuk dipanggil berulang-ulang  
                     //dan mempersingkat scrip.
    
                if($rupiah < 12)
                    return "  ". $angka[$rupiah];
                        //Return diatas adalah untuk memanggil angka yang kurang dari 12.  
                        //dan juga untuk memanggil angka spesial yaitu "sebelas".
                else if ($rupiah < 20)
                    return terbilang($rupiah - 10) ." ". "belas"; 
                        //return terbilang diatas adalah untuk memanggil angka kurang dari 20 
                        //dengan menambahkan kata "belas" dibelakangnya. 

                elseif ($rupiah < 100)
                    return terbilang($rupiah / 10) . " " . "Puluh" . terbilang ($rupiah  % 10);
                        //return terbilang diatas adalah untuk memanggil angka kurang dari 100.

                elseif ($rupiah < 200)
                    return " ". $iii . terbilang ($rupiah-100);
                        //memanggil variable iii agar saat menginputkan angka 100 maka output nya menampilkan "seratus".

                elseif ($rupiah < 1000)
                    return  terbilang($rupiah / 100) . " " . "Ratus" . terbilang ($rupiah % 100);
                        //return terbilang diatas adalah untuk memanggil angka kurang dari 1000.

                elseif ($rupiah < 2000)
                    return " ". $uuuu . terbilang ($rupiah-1000);
                        //memanggil variable uuuu agar saat menginputkan angka 1000 maka output nya menampilkan "seribu".

                elseif ($rupiah < 1000000)
                    return   terbilang($rupiah / 1000) . " " . "Ribu" . terbilang ($rupiah % 1000);
                        //return terbilang diatas adalah untuk memanggil angka kurang dari 1000000 
                        //dengan menampilkan kata "ribu" dibelakangnya 

                elseif ($rupiah <1000000000)
                    return terbilang($rupiah / 1000000) . " " . "Juta".  terbilang ($rupiah % 1000000);
                        //return terbilang diatas adalah untuk memanggil angka kurang dari 1000000000 
                        //dengan menampilkan kata "juta" di belakangnya

                elseif ($rupiah < 1000000000000)
                    return terbilang($rupiah / 1000000000) . " " . "Milyar". terbilang (fmod($rupiah , 1000000000));
                        //return terbilang diatas adalah untuk memanggil angka kurang dari 1000000000000
                        //agar menampilkan kata "milyar" dibelakangnya. dengan memanggil function fmod.
                        //fmod adalah untuk mencari nilai dari sisa bagi bilangan yang telah ditentukan pembilang dan pembagi
                
                 //nama dari isi () adalah parameter
                 //nama dari <> adalah delimeter   
                 
                }
                
            echo terbilang (1250000); //untuk menginput nilai. 
            //echo untuk menampilkan yang di inputkan
            //Proses = jika menginputkan angka maka system akan memproses dari atas untuk mengetahui angka tersebut adalah 
            //angka belasan,ratusan, ribuan , jutaan , atau ,milyaran ,dengan menampilkan angka yang di inputkan. contoh jika
            //menginputkan nilai 1250000 maka akan menampilkan satu juta dua ratus lima puluh ribu rupiah sesuai nominal angka tersebut.
            echo ("Rupiah");//untuk menampilkan kata rupiah.
?>