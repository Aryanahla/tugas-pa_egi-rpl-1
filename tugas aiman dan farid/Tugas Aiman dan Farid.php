<?php 
//Aiman Nursubkhi
	$tahun = new DateTime('2002-03-28');
	//Memanggil data yang telah di masukan.
	
	$tgl   = new DateTime();
	//Membuat fungsi zona waktu yang ada di sistem komputer atau mengambil zona waktu yang terdapat pada sistem komputer
	
	$usia  = date_diff($tgl , $tahun);
	//Membuat fungsi perbandingan zona waktu yang telah ditentukan sebelumnya.

	$tahun = $usia -> y;
	//Mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan tahun.

	$bulan = $usia -> m;
	//Mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan bulan.

	$hari  = $usia -> d;
	//Mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan hari.
	
	$jam   = $usia -> h;
	//Mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan jam.
	
	$menit = $usia -> i;
	//Mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan menit.
	
	$detik = $usia -> s;
	//Mengambil data yang ada didalam variabel usia yang berisikan perbandingan zona waktu yang menunjukan detik.

	echo (" USIA ANDA: " . "</br>" . $tahun . " Tahun " . $bulan . " Bulan " . $hari . " Hari " . $jam . " Jam " . $menit . " Menit " . $detik . " Detik");
	//Memanggil satu persatu variabel yang telah terisi data dari masing masing jenis waktu(hari, tahun, dan sebagainya)

	echo ("<hr>");
	//Membuat garis horizontal sebagai pembatas.
?>