<?php
    function terbilang ($rupiah){
    //function adalah baris pertama yg mempermudahkan kita dalam membuat program
    // function terbilang adalah Kode program yg  bisa kita kasih nama lalu bisa dipanggil berulang
        $angka = array ();
        // array adalah Variabel yang dapat memiliki banyak nilai / data
        $angka = [ "", "satu", "dua", "tiga", 
        "empat", "lima", "enam", "tujuh", "delapan",
         "sembilan", "sepuluh", "sebelas"];
         // $rupiah masuk ke dalam user defined karena kita membuat variabel sendiri 
         // variabel tidak boleh ada simbol dan angka

         if ($rupiah < 12)
         // < 12 karena di variabel angkanya cuma sebelas
          return " " . $angka[$rupiah];
          // return diatas berfungsi untuk pengirim value (nilai) isi
            elseif( $rupiah < 20)
          // elseif adalah suatu keadaan yang mempunyai dua kemungkinan
              return terbilang ($rupiah - 10) . " " . "belas ";
            elseif ($rupiah < 100)
              return terbilang ($rupiah/10) . " " . "puluh " . terbilang ($rupiah % 10);
            elseif ($rupiah  < 200)
              return "seratus " . terbilang ($rupiah - 100);
            elseif ($rupiah < 1000)
             return terbilang ($rupiah/100) . " " . "ratus " . terbilang ($rupiah % 100);
            elseif ($rupiah  < 2000)
              return "seribu " . terbilang ($rupiah - 1000);
            // return "seribu" disebut spesial karena jika tidak dibuat akan menampilkan satu ribu
            elseif ($rupiah < 1000000)
              return terbilang ($rupiah/1000) . " " . "ribu " . terbilang ($rupiah % 1000);
             // return ribu akan menampilkan ribu karena kurang dari satu juta
            else if ($rupiah < 1000000000)
              return terbilang ($rupiah/1000000) . " " . "juta " . terbilang ($rupiah % 1000000);
            // return juta akan menampilkan juta karena kurang dari satu milyar
            else if ($rupiah < 1000000000000)
              return terbilang ($rupiah/1000000000) . " " . "milyar " . terbilang(fmod($rupiah, 1000000000));
            else if ($rupiah < 1000000000000000)
              return terbilang ($rupiah/1000000000000) . " " . "triliyun " . terbilang(fmod($rupiah, 1000000000000));
              // fmod berfungsi untuk melebihi batas dari modulus
              // jadi else berfungsi untuk
              
                   
    }
    echo terbilang (1001250000);
    // 200 ribu masuk ke ratus karena kurang dari 1000
    // 50 ribu masuk ke puluh karena kurang dari 100
    // isi dari () disebut parameter
    // <> disebut delimeter
    echo (" rupiah ")
    // echo berfungsi untuk menampilkan
    // >< == tanda seperti itu dinamakan logika

?>