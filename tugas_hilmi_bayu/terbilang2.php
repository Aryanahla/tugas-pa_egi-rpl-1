<?php 

function terbilang ($rupiah){ //function adalah sebuah kode dari program yang memuat sekumpulan proses yang bisa dipanggil secara berulang dan m
$rupiah = abs($rupiah); // function abs berfungsi untuk mengembalikan nilai negatif ke nilai positif, misalkan inputnya -500, maka dengan adanya abs akan berubah menjadi 500
//$angka = array();
$angka = array ("", "satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan",
            "sepuluh", "sebelas"); //Array adalah variabel yang didalamnya terdapat banyak data
                                    //Array selalu dimulai dari 0 maka pada array diatas, diawali dengan string kosong atau "" supaya ketika di inputkan "1" akan langsung mengambil string "Satu" tetapi bila langsung diawali dengan string "Satu" maka ketika diinputkan 1 akan muncul string "Dua"



if ($rupiah < 12) {
    return " ". $angka[$rupiah];// If ini berfungsi menyaring $rupiah apabila kurang dari 12, maka akan langsung mengambil data yang ada didalam $angka sesuai dengan nilai yang diinput. contoh bila $rupiah berisi 11 maka akan langsung mengambil data dari $angka yaitu "Sebelas"
}else if ($rupiah < 20){ 
    return terbilang($rupiah - 10) . " belas";// If ini berfungsi menyaring $rupiah apabila kurang dari 20, maka $rupiah tersebut dikurang 10 lalu diambil string dari $angka dan ditambahkan string "belas". contoh bila $rupiah berisi 15 maka akan dikurangi 10 menjadi 5 "Lima" lalu ditambahkan string "belas" menjadi "Lima Belas"
}else if  ($rupiah < 100){
    return  terbilang($rupiah / 10) . " puluh" . terbilang($rupiah % 10); // If ini berfungsi menyaring $rupiah apabila kurang dari 100, maka $rupiah tersebut dibagi 10 dan ditambah string "puluh" lalu $rupiah tersebut di modulus 10. contoh bila $rupiah berisi 62 maka akan dibagi 10 menjadi 6 "Enam" terus ditambah string "Puluh" lalu 62 tadi di modulus 10 jadi sisa 2 "Dua" menjadi "Enam Puluh Dua"
}else if ($rupiah < 200){
    return " seratus" . terbilang($rupiah - 100 ); // If ini berfungsi menyaring $rupiah apabila kurang dari 200, maka $rupiah tersebut diawali string "Seratus" lalu diikuti $rupiah angka yang sudah dikurangi 100. contoh bila $rupiah berisi 140, maka otomatis ditambahkan string "Seratus" lalu $rupiah yg berisi 140 dikurangi 100 menjadi 40, nah 40 ini masuk ke else if yg kurang dari 100, 40 dibagi 10 menjadi 4 "empat" ditambah string "puluh" karena angka 0 di belakang, maka tidak perlu di proses karena tidak menghasilkan sisa bagi. hasil keseluruhannya adalah "Seratus empat puluh"
}else if ($rupiah < 1000){
    return terbilang($rupiah / 100) . " ratus" . terbilang($rupiah % 100); //if ini berfungsi menyaring $rupiah apabila kurang dari seribu, maka $rupiah tersebut dibagi 100 lalu ditambahkan string " ratus" dan ditambahkan lagi $rupiah yang sudah di Modulus 100 lalu $rupiah yang telah di modulus 100 tersebut mengambil string dari array $angka yang sesuai dengan hasil modulusnya. contoh bila $rupiah berisi 566, maka 566 tersebut dibagi 100, menjadi 5 "Lima", kemudian ditambahkan string " ratus" lalu $rupiah yang berisi 566 tersebut di modulus 100, hasilnya 66. nah 66 tersebut masuk pada kondisi elseif $rupiah kurang dari 100, itu berarti 66 dibagi 10, hasilnya 6 "enam". lalu ditambah string "puluh" dan $rupiah yang berisi 66 tersebut dimodulus 10 hasilnya menjadi 6 "enam" maka outputnya menjadi "lima ratus enam puluh enam". 
}else if ($rupiah < 2000){
    return "seribu" . terbilang($rupiah - 1000); //if pada kondisi ini berfungsi untuk menyaring $rupiah apabila kurang dari 2000, maka $rupiah tersebut ditambahkan string "Seribu" di awal, lalu $rupiah yang bernilai kurang dari 2000 akan dikurang 1000. Contoh bila $rupiah berisi 1300, maka ditambahkan "seribu" diawal, lalu 1300 tersebut dikurangi 1000 jadi hasilnya 300. Nah 300 ini masuk pada kondisi elseif $rupiah kurang dari 1000 dan di proses sesuai proses yang ada didalam kondisi tersebut. 
}else if ($rupiah < 1000000){
    return terbilang($rupiah / 1000) . " ribu" . terbilang($rupiah % 1000); //if pada kondisi ini berfungsi untuk menyaring $rupiah apabila kurang dari 1000000, maka $rupiah tersebut dibagi "1000" lalu ditambahkan "ribu" dan $rupiah tersebut di modulus 1000. Contoh, bila $rupiah berisi 465000, maka 465000 tersebut dibagi 1000 menjadi 465, lalu 465 itu masuk ke kondisi elseif $rupiah kurang dari 1000, begitu seterusnya sebagaimana fungsi rekursif. setelah selesai akan menghasilkan "Empat ratus enam puluh lima" ditambah string "ribu", lalu $rupiah tersebut dimodulus 1000, bila mempunyai sisa bagi maka akan ditambahkan setelah "ribu" jika tidak maka proses selesai sampai disitu. 
}else if ($rupiah < 1000000000){
    return terbilang ($rupiah / 1000000) . " juta" . terbilang ($rupiah % 1000000); // if pada kondisi ini berfungsi untuk menyaring $rupiah apabila kurang dari 1000000000, lalu $rupiah tersebut di bagi 1000000 dan ditambahkan string " juta", kemudian dimodulus 1000000. contoh bila $rupiah berisi 1500000, maka dibagi 1000000 menjadi 1 "satu" ditambah string "juta" lalu $rupiah yang berisi 1500000 dimodulus 1000000 hasilnya adalah 500000, nah 500000 ini masuk kedalam kondisi elseif $rupiah kurang dari 1000000, yaitu 500000 dibagi 1000 hasilnya 500, 500 ini akan diproses di elseif $rupiah kurang dari 1000, dan seterusnya sebagaimana fungsi rekursif. hasil akhirnya menjadi "Satu juta lima ratus"
}else if ($rupiah < 1000000000000){ 
    return terbilang ($rupiah / 1000000000) . " milyar" . terbilang (fmod($rupiah , 1000000000)); //if pada kondisi ini berfungsi untuk menyaring $rupiah apabila kurang dari 1000000000000 lalu $rupiah tersebut dibagi 1000000000 dan ditambahkan string "Milyar", kemudian dimodulus 1000000000. Contoh bila $rupiah berisi 5600000000, maka dibagi 1000000000 menjadi 5 "lima", ditambah string "milyar" lalu 5600000000 dimodulus 1000000000 dan menghasilkan 600000000, 600000000 tersebut akan diproses elseif $rupiah kurang dari 1000000000 dan seterusnya. outputnya akan menghasilkan "lima milyar enam ratus juta"
}else if ($rupiah < 1000000000000000){
    return terbilang ($rupiah / 1000000000000) . " trilyun" . terbilang (fmod($rupiah , 1000000000000)); //if pada kondisi ini berfungsi untuk menyaring $rupiah apabila kurang dari 1000000000000000 lalu $rupiah tersebut dibagi 1000000000000 dan ditambahkan string "Trilyun", kemudian dimodulus 1000000000000. Contoh bila $rupiah berisi 4000000000000, maka $rupiah tersebut dibagi 1000000000000 meenghasilkan 4 "empat", lalu ditambah string "Trilyun", setelah itu $rupiah di Modulus 1000000000000 tetapi tidak menghasilkan sisa bagi, maka proses telah selesai. outputnya adalah "Empat Trilyun"
} // return berfungsi untuk mengembalikan nilai 

}

function mines ($rupiah){ // function ini berfungsi menambahkan String "Minus" pada output bila input tersebut kurang dari 0

    if ($rupiah < 0){
        $hasil = "minus " . terbilang($rupiah);
    }else {
        $hasil = terbilang($rupiah);
    }
    return $hasil;
}
$angka1 = 100225785498000;
echo mines ($angka1); //echo mines akan menampilkan angka yang telah ditampung oleh function mines, angka tersebut berasal dari variabel angka1 


?>